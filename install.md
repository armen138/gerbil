# Build and install Gerbil

Gerbil uses the meson build system. While Gerbil itself is written in python, certain dependencies may need to be compiled in C/C++.
If meson is installed

```
meson build
cd build
meson compile
meson install
```

In order for Gerbil to run properly, some files are expected to be in their installed locations. The application may not run well, or at all, if you do not install it before running.

# Build flatpak

The CI configuration(gitlab-ci.yml) uses the following steps to build the flatpak package:

```
flatpak install -y flathub org.gnome.Sdk//3.38 org.gnome.Platform//3.38
flatpak-builder app com.armen138.gerbil.json
flatpak build-export --arch=x86_64 repo app
flatpak build-bundle --arch=x86_64 repo com.armen138.gerbil.flatpak com.armen138.gerbil master
```

To build the package for other architectures, you may need to install qemu as well.
Note that the flatpak manifest defines the gitlab repository as the source, so that will be used to build rather than the local sources.

The latest flatpak builds should also be available as downloadable artifacts from gitlab-ci:

For *x86_64*: https://gitlab.com/armen138/gerbil/-/jobs/artifacts/master/download?job=x86_64
For *aarch64 (pinephone, etc)*: https://gitlab.com/armen138/gerbil/-/jobs/artifacts/master/download?job=aarch64

## Known issues using flatpak to install

The flatpak installation packages its own Gtk 3. This means it will not include Gtk patches normally applied on Phosh to make certain widgets more mobile-friendly.
In practical terms, this means the "about" screen may not quite fit the display on the pinephone at the default scaling setting (2x).
Despite this, flatpak is still the recommended way to install Gerbil.
