# Gerbil

[![pipeline status](https://gitlab.com/armen138/gerbil/badges/master/pipeline.svg)](https://gitlab.com/armen138/gerbil/-/commits/master)

``` gerbil icon ascii art
                 `:?c7v|'                         
             -|u]7;;|zZNQKDRgNNRkt>'              
           :wu_ `'::'`         `,^zOQE^           
         `eP'                       `;qQL`        
        ,8+                            ,KQr       
       ^g,    '\='                 `"    ;Qa`     
      iM`     x:`Lr``:\\?>|\=+?+''/J;k    'QK     
     Lg`      ,Q#Jou?:-        :Z#zWc$     :@i    
    ,Q'        jgOZ+;-':?/|,,^7+=SbK7`      u@-   
    Zz       c7_^iv=;\Q@@@@K=~WQQOcZ^       "@\   
    Q_     `jrLBQ@QRNz~66eb;;QQ@@QQ}b`      `@S   
   '@`     iE'kQ@@NN@& s` /+#U@@Q#Sdg`       @e   
   ,@^=\\=;|Q_w@@@Q@@|,t+vPB#DQQN@k@B       '@J   
;//L@r.``.,^v#NQQ9b]zeh27:;j92g#gQ@@Qujjjyy2k@2;'`
'   DL;||?>;iQ=;\/jPR{^9@@@jegM$j;e@@'      D#:^c2
  `=q#,    `eQ=:_:~:_`-NgQq:,_:;;:^@@v:`   ;@|    
-Lc'-@, ;^;:^@gz^_:;uSjfFJL\\?r'';^o@\';^^~#Q     
|`   XQ|`    -?loWQ@Q;   `-;Pwow=7eQBv;   m@x'    
   ;|'jR`          ^Q@QRjdQNQ{:\zt/;` ,L^t@; ;\:  
 'l,   |N;          _/FPRQQQ\          `R@v    :l.
r|      'eq;                          |Qe.\\     \
:         ,SD7'                    ~z#k:   ^u     
            `reqj>'          `,;chRk\'      :F    
                '^zemmewXwPEkXF\;`           ^v   
                                              v,  
```

Gerbil is a little rodent. And also a little gemini browser, written with mobile linux in mind!

## Why?

Well, mostly because I can. Gemini is a fascinating protocol and markup format, in my opinion worthy of getting involved in.
In addition, I want to do more to contribute to mobile-friendly linux applications, and this seems like a good start!

## Features

* Mobile linux friendly
* Browse documents served over the gemini protocol
* Render gmi markup
* Render text files
* Passes all tests in gemini://gemini.conman.org/test/torture/
* Bookmarks
* Url preview on links for desktop/mouse users on hover
* Input requests
* Ask OS to open non-gemini links
* Download files that aren't gmi/text/xml
* Search from address bar
* Support for the spartan protocol: gemini://spartan.mozz.us
* Render diff content
* Manage server identities (gemini://makeworld.space/gemlog/2020-07-03-tofu-rec.gmi)

### TODO

* Manage identities (client certificates)
* Serve CSV/TSV as tables with sorting/filtering
* Color/font themes
* Add favicons support as proposed here: gemini://mozz.us/files/rfc_gemini_favicon.gmi
* Add url preview to links, make this optional
* Add document index navigation
* Do something about ANSI escape codes
* Render markdown
* Render Atom/RSS feeds
* Render gpub (gmi based ebook format)
* Support streaming gmi to make this work: gemini://chat.mozz.us

## Links referenced in this document

=> gemini://gemini.conman.org/test/torture/ Gemini client torture test  
=> gemini://spartan.mozz.us Spartan protocol  
=> gemini://mozz.us/files/rfc_gemini_favicon.gmi Favicon support proposal  
=> gemini://chat.mozz.us A "chat" using long-running TCP connections to stream content  
=> gemini://makeworld.space/gemlog/2020-07-03-tofu-rec.gmi TOFU recommendations  

> yes, this document is a gemini document pretending to markdown
