from gi.repository import GLib, Gtk, Pango
import os
import json

class Settings:
    items = {}
    filename = os.path.join(GLib.get_user_data_dir(), "settings.json")
    def __init__(self):
        if os.path.exists(self.filename):
            with open(self.filename, "r") as settings_file:
                self.items = json.loads(settings_file.read())

    def set_item(self, item_name, item_value):
        self.items[item_name] = item_value
        with open(self.filename, "w") as settings_file:
            settings_file.write(json.dumps(self.items))

    def get_item(self, item_name):
        if item_name in self.items:
            return self.items[item_name]
        return None
