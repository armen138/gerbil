from gi.repository import Gtk, Pango
import re
import html

DIFF_HEADER_PATTERN = re.compile("diff.*\d\s@@\s")

class Diff:
    def __init__(self, text):
        self.text = text
    def as_markup(self):
        text = html.escape(self.text)
        tokens = re.split(r"(diff --|@@.*?@@)", text)
        print(tokens)
        diff_header = False
        markup = ""
        for token in tokens:
            if token.startswith("diff --"):
                markup = markup + "<span color='gray'>" + token
                diff_header = True
            elif not diff_header:
                lines = token.split("\n")
                for line in lines:
                    if line.startswith("+"):
                        markup = markup + f"<span color='green'>{line}</span>\n"
                    elif line.startswith("-"):
                        markup = markup + f"<span color='red'>{line}</span>\n"
                    elif line.startswith("@@") and line.endswith("@@"):
                        markup = markup + f"<span color='grey'>{line}</span>\n"
                    else:
                        markup = markup + line + "\n"
            elif diff_header and token.endswith("@@"):
                markup = markup + token + "</span>"
                diff_header = False
            elif diff_header:
                markup += token
        print(markup)
        return f"<tt>{markup}</tt>"
    def as_buffer(self):
        markup = self.as_markup()
        tb = Gtk.TextBuffer.new()
        tb.insert_markup(tb.get_end_iter(), markup, -1)
        return tb
