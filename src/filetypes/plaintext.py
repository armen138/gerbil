from gi.repository import Gtk, Pango

class PlainText:
    def __init__(self, text):
        self.text = text
    def as_buffer(self):
        tb = Gtk.TextBuffer.new()
        tb.set_text(self.text)
        return tb
