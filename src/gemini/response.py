class GeminiResponse:
    def __init__(self, uri, status, meta, body = None, mime = "text/gemini", lang = None):
        self.uri = uri
        self.status = status
        self.meta = meta
        self.body = body
        self.mime = mime
        self.lang = lang

    def is_gmi(self):
        return "gemini" in self.mime

    def is_text(self):
        return "text/plain" in self.mime

    def is_markdown(self):
        return "markdown" in self.mime

    def is_image(self):
        return "image" in self.mime

